# Wire for Franz
Community-contributed Franz recipe for Wire. https://app.wire.com

## How to create your own Franz recipes:
* [Read the documentation](https://github.com/meetfranz/plugins)
